#!/bin/bash

set -euo pipefail

DIR=$(cd $(dirname "${BASH_SOURCE[0]}") &> /dev/null; pwd)
ROOT_DIR=$(readlink -f ${ROOT_DIR:-$DIR/..})
CONTAINER=csv-fish-windows
PE="podman exec $CONTAINER"
PEB="podman exec $CONTAINER bash -c"

function start() {
    if podman ps -a | grep csv-fish-windows; then
        podman start csv-fish-windows
    else
        podman run -d --name csv-fish-windows -v "$ROOT_DIR:/project:z" registry.fedoraproject.org/fedora:29 sleep infinity

        $PE dnf -y install gcc mingw64-gcc mingw64-winpthreads-static mingw64-sqlite-static
        $PEB 'curl https://sh.rustup.rs -sSf | sh -s -- -y'
        $PEB 'source $HOME/.cargo/env && rustup target add x86_64-pc-windows-gnu'
        $PEB 'echo "[target.x86_64-pc-windows-gnu]" > ~/.cargo/config'
        $PEB 'echo "linker = \"x86_64-w64-mingw32-gcc\"" >> ~/.cargo/config'
        $PEB 'echo "ar = \"x86_64-w64-mingw32-gcc-ar\"" >> ~/.cargo/config'
    fi
}

function build() {
    $PEB 'cd /project && source $HOME/.cargo/env && cargo build --release --target=x86_64-pc-windows-gnu --verbose'
}

function stop() {
    podman stop csv-fish-windows
}

start && build && stop || stop

echo
echo "If you want to remove the build container, run 'podman rm $CONTAINER'."
