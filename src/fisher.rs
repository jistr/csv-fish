use csv::{Writer, WriterBuilder};
use fishers_exact::{fishers_exact, FishersExactPvalues};
use std::fs::File;
use std::path::Path;

use crate::SimpleResult;
use crate::group::{TestGroup, TestGroupCount};

pub type Pvalues = FishersExactPvalues;

pub fn pvalues(group_counts: &[TestGroupCount]) -> SimpleResult<Vec<Pvalues>> {
    println!("Computing Fisher's exact test p-values");
    let mut pvalues = vec![];
    for count in group_counts.iter() {
        pvalues.push(fishers_exact(&[count.r1c1, count.r1c2, count.r2c1, count.r2c2])?)
    }
    Ok(pvalues)
}

pub fn write_results(groups: &[TestGroup], group_counts: &[TestGroupCount], pvalues: &[Pvalues],
                     path: &Path) -> SimpleResult<()> {
    println!("Writing results");
    let mut writer = create_csv_writer(path)?;
    writer.write_record(vec![
        "filter",
        "r1cond",
        "r2cond",
        "c1cond",
        "c2cond",
        "r1c1",
        "r1c2",
        "r2c1",
        "r2c2",
        "fisher_l",
        "fisher_r",
        "fisher_2t",
    ])?;

    for (i, group) in groups.iter().enumerate() {
        let count = &group_counts[i];
        let pvalue = &pvalues[i];

        let record: Vec<String> = vec![
            group.filter.as_ref().cloned().unwrap_or("".to_string()),
            group.r1cond.clone(),
            group.r2cond.as_ref().cloned().unwrap_or("".to_string()),
            group.c1cond.clone(),
            group.c2cond.as_ref().cloned().unwrap_or("".to_string()),
            count.r1c1.to_string(),
            count.r1c2.to_string(),
            count.r2c1.to_string(),
            count.r2c2.to_string(),
            pvalue.less_pvalue.to_string(),
            pvalue.greater_pvalue.to_string(),
            pvalue.two_tail_pvalue.to_string(),
        ];

        writer.write_record(&record)?;
    }

    Ok(())
}

fn create_csv_writer(path: &Path) -> SimpleResult<Writer<File>> {
    WriterBuilder::new()
        .delimiter(b';')
        .from_path(path).map_err(|e| e.into())
}
