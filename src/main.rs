mod db;
mod data;
mod fisher;
mod group;
mod result;

use clap::{App, Arg};
use std::path::Path;
use std::process::exit;

use db::DB;

pub use crate::result::SimpleResult;

const VERSION: Option<&'static str> = option_env!("CARGO_PKG_VERSION");

fn main() {
    exit(match run() {
       Ok(_) => 0,
       Err(err) => {
           eprintln!("Error: {:?}", err);
           1
       }
    });
}

fn run() -> SimpleResult<()> {
    let app = App::new("CSV Fish")
        .version(VERSION.unwrap_or("unknown"))
        .author("by Kamila Stránská & Jiří Stránský")
        .about("CSV categorical data analyzer. Generates 2x2 contingency tables \n\
                and applies Fisher's exact test. See the documentation at\n\
                https://gitlab.com/jistr/csv-fish/blob/main/README.md")
        .arg(Arg::with_name("data")
             .long("data")
             .takes_value(true)
             .value_name("FILE")
             .help("Data CSV file")
             .required(true))
        .arg(Arg::with_name("groups")
             .long("groups")
             .takes_value(true)
             .value_name("FILE")
             .help("Groups CSV file")
             .required(true))
        .arg(Arg::with_name("output")
             .long("output")
             .takes_value(true)
             .value_name("FILE")
             .help("Results CSV file to be written")
             .required(true));

    let matches = app.get_matches();

    let data_path = Path::new(matches.value_of("data").expect("Parameter 'data' missing."));
    let data_headers = data::read_csv_headers(&data_path)?;
    let db = DB::new_with_columns(&data_headers)?;
    data::read_and_populate_db(&db, &data_path)?;

    let groups_path = Path::new(matches.value_of("groups").expect("Parameter 'groups' missing."));
    let groups = group::read_groups(&groups_path)?;
    let group_counts = group::query_group_counts(&db, &groups)?;
    let pvalues = fisher::pvalues(&group_counts)?;

    let output_path = Path::new(matches.value_of("output").expect("Parameter 'output' missing."));
    fisher::write_results(&groups, &group_counts, &pvalues, &output_path)?;

    println!("Success");
    Ok(())
}
