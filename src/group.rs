use csv::{Reader, ReaderBuilder};
use std::fs::File;
use std::path::Path;

use crate::SimpleResult;
use crate::db::DB;

#[derive(Debug, PartialEq)]
pub struct TestGroupCount {
    pub r1c1: u32,
    pub r1c2: u32,
    pub r2c1: u32,
    pub r2c2: u32,
}

#[derive(Debug, PartialEq)]
pub struct TestGroup {
    pub filter: Option<String>,
    pub r1cond: String,
    pub r2cond: Option<String>,
    pub c1cond: String,
    pub c2cond: Option<String>,
}

#[derive(Debug, PartialEq)]
struct GroupInput {
    condtype: String,
    filter: String,
    r1cond: String,
    r2cond: String,
    c1cond: String,
    c2cond: String,
}

struct GroupInputTypes {
    row: Vec<GroupInput>,
    col: Vec<GroupInput>,
    rowcol: Vec<GroupInput>,
}
impl GroupInputTypes {
    pub fn new() -> Self {
        Self {
            row: vec![],
            col: vec![],
            rowcol: vec![],
        }
    }
}

pub fn read_groups(path: &Path) -> SimpleResult<Vec<TestGroup>> {
    println!("Creating row/column combinations");
    let group_input_types = read_group_input_types(path)?;
    let rowcol_group_inputs = convert_to_rowcol(group_input_types)?;
    convert_to_groups(rowcol_group_inputs)
}

pub fn query_group_counts(db: &DB, groups: &[TestGroup]) -> SimpleResult<Vec<TestGroupCount>> {
    println!("Creating contingency tables");
    let mut counts = vec![];
    for group in groups.iter() {
        counts.push(TestGroupCount {
            r1c1: db.count(&combined_condition(
                &group.filter,
                &group.r1cond,
                &group.c1cond,
            ))?,
            r1c2: db.count(&combined_condition(
                &group.filter,
                &group.r1cond,
                &derived_neg_condition(&group.c1cond, &group.c2cond),
            ))?,
            r2c1: db.count(&combined_condition(
                &group.filter,
                &derived_neg_condition(&group.r1cond, &group.r2cond),
                &group.c1cond,
            ))?,
            r2c2: db.count(&combined_condition(
                &group.filter,
                &derived_neg_condition(&group.r1cond, &group.r2cond),
                &derived_neg_condition(&group.c1cond, &group.c2cond),
            ))?,
        });
    }
    Ok(counts)
}

fn combined_condition(filter: &Option<String>, row: &str, col: &str) -> String {
    let filter_part = match filter {
        None => "".to_string(),
        Some(filter_cond) => format!("{} AND ", filter_cond),
    };
    format!("{}{} AND {}", filter_part, row, col)
}

fn derived_neg_condition(pos_cond: &str, neg_cond: &Option<String>) -> String {
    match neg_cond {
        None => format!("NOT ({})", pos_cond),
        Some(cond) => cond.clone(),
    }
}

fn read_group_input_types(path: &Path) -> SimpleResult<GroupInputTypes> {
    let mut reader = create_csv_reader(path)?;
    let headers = reader.headers()?;
    let expected_headers = vec!["condtype", "filter", "r1cond", "r2cond", "c1cond", "c2cond"];
    if headers != expected_headers {
        return Err(format!("Incorrect group CSV headers. Expected {:?} but got {:?}",
                           expected_headers, headers).into())
    }

    let mut group_input_types = GroupInputTypes::new();
    let mut records = reader.into_records();
    while let Some(rec_result) = records.next() {
        let rec = rec_result?;
        let condtype = &rec[0];
        let gi = GroupInput {
            condtype: condtype.to_string(),
            filter: rec[1].to_string(),
            r1cond: rec[2].to_string(),
            r2cond: rec[3].to_string(),
            c1cond: rec[4].to_string(),
            c2cond: rec[5].to_string(),
        };
        match condtype {
            "row" => group_input_types.row.push(gi),
            "col" => group_input_types.col.push(gi),
            "rowcol" => group_input_types.rowcol.push(gi),
            _ => return Err(format!("Unknown value in condtype column: '{}'", condtype).into()),
        }
    }
    Ok(group_input_types)
}

fn convert_to_rowcol(git: GroupInputTypes) -> SimpleResult<Vec<GroupInput>> {
    // Speed up by reusing the rowcol vector - we're the owner now.
    let mut rowcol = git.rowcol;
    for r in git.row.iter() {
        for c in git.col.iter() {
            let filter: String = if r.filter != "" && c.filter != "" {
                format!("({}) AND ({})", r.filter, c.filter)
            } else if r.filter != "" {
                r.filter.clone()
            } else if c.filter != "" {
                c.filter.clone()
            } else {
                "".to_string()
            };

            rowcol.push(GroupInput {
                condtype: "rowcol".to_string(),
                filter: filter,
                r1cond: r.r1cond.clone(),
                r2cond: r.r2cond.clone(),
                c1cond: c.c1cond.clone(),
                c2cond: c.c2cond.clone(),
            });
        }
    }
    Ok(rowcol)
}

fn convert_to_groups(rowcol_inputs: Vec<GroupInput>) -> SimpleResult<Vec<TestGroup>> {
    let mut groups = vec![];
    for input in rowcol_inputs.into_iter() {
        if input.condtype != "rowcol" {
            return Err("Only rowcol condition types are allowed in convert_to_groups".into());
        }

        groups.push(TestGroup {
            filter: match input.filter.as_ref() { "" => None, _ => Some(input.filter) },
            r1cond: input.r1cond,
            r2cond: match input.r2cond.as_ref() { "" => None, _ => Some(input.r2cond) },
            c1cond: input.c1cond,
            c2cond: match input.c2cond.as_ref() { "" => None, _ => Some(input.c2cond) },
        })
    }
    Ok(groups)
}

fn create_csv_reader(path: &Path) -> SimpleResult<Reader<File>> {
    ReaderBuilder::new()
        .delimiter(b';')
        .from_path(path).map_err(|e| e.into())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_convert_to_rowcol() {
        let mut git = GroupInputTypes::new();
        git.row.push(GroupInput {
            condtype: "row".to_string(),
            filter: "property1 = 'row1filter'".to_string(),
            r1cond: "property2 = 'r1cond'".to_string(),
            r2cond: "".to_string(),
            c1cond: "".to_string(),
            c2cond: "".to_string(),
        });
        git.row.push(GroupInput {
            condtype: "row".to_string(),
            filter: "".to_string(),
            r1cond: "property2 = 'r2cond'".to_string(),
            r2cond: "property2 != 'r2cond'".to_string(),
            c1cond: "".to_string(),
            c2cond: "".to_string(),
        });
        git.col.push(GroupInput {
            condtype: "col".to_string(),
            filter: "property3 = 'col1filter'".to_string(),
            r1cond: "".to_string(),
            r2cond: "".to_string(),
            c1cond: "property4 = 'c1cond'".to_string(),
            c2cond: "".to_string(),
        });
        git.col.push(GroupInput {
            condtype: "col".to_string(),
            filter: "".to_string(),
            r1cond: "".to_string(),
            r2cond: "".to_string(),
            c1cond: "property4 = 'c2cond'".to_string(),
            c2cond: "property4 != 'c2cond'".to_string(),
        });
        git.rowcol.push(GroupInput {
            condtype: "rowcol".to_string(),
            filter: "".to_string(),
            r1cond: "property5 = 'rc1cond'".to_string(),
            r2cond: "".to_string(),
            c1cond: "property4 = 'rc1cond'".to_string(),
            c2cond: "".to_string(),
        });

        let rowcols = convert_to_rowcol(git).expect("Failed to convert to rowcols");
        assert_eq!(rowcols, vec![
            GroupInput {
                condtype: "rowcol".to_string(),
                filter: "".to_string(),
                r1cond: "property5 = 'rc1cond'".to_string(),
                r2cond: "".to_string(),
                c1cond: "property4 = 'rc1cond'".to_string(),
                c2cond: "".to_string(),
            },
            GroupInput {
                condtype: "rowcol".to_string(),
                filter: "(property1 = 'row1filter') AND (property3 = 'col1filter')".to_string(),
                r1cond: "property2 = 'r1cond'".to_string(),
                r2cond: "".to_string(),
                c1cond: "property4 = 'c1cond'".to_string(),
                c2cond: "".to_string(),
            },
            GroupInput {
                condtype: "rowcol".to_string(),
                filter: "property1 = 'row1filter'".to_string(),
                r1cond: "property2 = 'r1cond'".to_string(),
                r2cond: "".to_string(),
                c1cond: "property4 = 'c2cond'".to_string(),
                c2cond: "property4 != 'c2cond'".to_string(),
            },
            GroupInput {
                condtype: "rowcol".to_string(),
                filter: "property3 = 'col1filter'".to_string(),
                r1cond: "property2 = 'r2cond'".to_string(),
                r2cond: "property2 != 'r2cond'".to_string(),
                c1cond: "property4 = 'c1cond'".to_string(),
                c2cond: "".to_string(),
            },
            GroupInput {
                condtype: "rowcol".to_string(),
                filter: "".to_string(),
                r1cond: "property2 = 'r2cond'".to_string(),
                r2cond: "property2 != 'r2cond'".to_string(),
                c1cond: "property4 = 'c2cond'".to_string(),
                c2cond: "property4 != 'c2cond'".to_string(),
            },
        ]);
    }

    #[test]
    fn test_convert_to_groups() {
        let inputs = vec![
            GroupInput {
                condtype: "rowcol".to_string(),
                filter: "property1 = 'row1filter'".to_string(),
                r1cond: "property2 = 'r1cond'".to_string(),
                r2cond: "property2 != 'r1cond'".to_string(),
                c1cond: "property4 = 'c2cond'".to_string(),
                c2cond: "property4 != 'c2cond'".to_string(),
            },
            GroupInput {
                condtype: "rowcol".to_string(),
                filter: "".to_string(),
                r1cond: "property2 = 'r1cond'".to_string(),
                r2cond: "".to_string(),
                c1cond: "property4 = 'c2cond'".to_string(),
                c2cond: "".to_string(),
            },
        ];

        let groups = convert_to_groups(inputs).expect("Failed to convert to groups");
        assert_eq!(groups, vec![
            TestGroup {
                filter: Some("property1 = 'row1filter'".to_string()),
                r1cond: "property2 = 'r1cond'".to_string(),
                r2cond: Some("property2 != 'r1cond'".to_string()),
                c1cond: "property4 = 'c2cond'".to_string(),
                c2cond: Some("property4 != 'c2cond'".to_string()),
            },
            TestGroup {
                filter: None,
                r1cond: "property2 = 'r1cond'".to_string(),
                r2cond: None,
                c1cond: "property4 = 'c2cond'".to_string(),
                c2cond: None,
            },
        ]);
    }

    #[test]
    fn test_combined_condition() {
        assert_eq!(
            "property1 = 'row1filter' AND property2 = 'r1cond' AND property3 = 'c1cond'",
            &combined_condition(
                &Some("property1 = 'row1filter'".to_string()),
                "property2 = 'r1cond'",
                "property3 = 'c1cond'"));
        assert_eq!(
            "property2 = 'r1cond' AND property3 = 'c1cond'",
            &combined_condition(
                &None,
                "property2 = 'r1cond'",
                "property3 = 'c1cond'"));
    }

    #[test]
    fn test_derived_neg_condition() {
        assert_eq!(
            "property2 != 'r1cond'",
            &derived_neg_condition(
                "property2 = 'r1cond'",
                &Some("property2 != 'r1cond'".to_string())));
        assert_eq!(
            "NOT (property2 = 'r1cond')",
            &derived_neg_condition(
                "property2 = 'r1cond'",
                &None));
    }
}
