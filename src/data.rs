use csv::{Reader, ReaderBuilder};
use std::fs::File;
use std::path::Path;

use crate::SimpleResult;
use crate::db::DB;

pub fn read_and_populate_db(db: &DB, path: &Path) -> SimpleResult<()> {
    println!("Populating the database");
    let mut insert_stmt = db.insert_stmt()?;
    let mut records = create_csv_reader(path)?.into_records();
    db.begin()?;
    while let Some(rec_result) = records.next() {
        let rec = rec_result?;
        let rec_ws_trim: Vec<String> = rec.iter().map(|s| s.trim().to_string()).collect();
        insert_stmt.execute(&rec_ws_trim)?;
    }
    db.end()?;
    Ok(())
}

pub fn read_csv_headers(path: &Path) -> SimpleResult<Vec<String>> {
    let mut reader = create_csv_reader(path)?;
    let headers = reader.headers()?;
    Ok(headers.iter().map(|s| s.to_string()).collect())
}

fn create_csv_reader(path: &Path) -> SimpleResult<Reader<File>> {
    ReaderBuilder::new()
        .delimiter(b';')
        .from_path(path).map_err(|e| e.into())
}
