use std::iter;
use std::fmt::Display;
use rusqlite::{Connection, NO_PARAMS, Statement};

use crate::SimpleResult;

pub struct DB {
    pub conn: Connection,
    pub columns: Vec<String>,
}

impl DB {
    pub fn new_with_columns<S>(columns: &[S]) -> SimpleResult<Self>
    where S: AsRef<str>+Display {
        let conn = Connection::open_in_memory()?;
        let vec_cols: Vec<String> = columns.iter().map(|s| s.to_string()).collect();

        println!("Creating in-memory database with columns: {}", vec_cols.join(", "));
        let column_sql: Vec<String> = vec_cols.iter().map(|col_name| format!("{} TEXT", col_name))
            .collect();
        let create_table = format!("CREATE TABLE data ({});", &column_sql.join(", "));
        conn.execute(&create_table, NO_PARAMS)?;
        Ok(DB {
            conn: conn,
            columns: vec_cols,
        })
    }

    pub fn insert_stmt<'a>(&'a self) -> SimpleResult<Statement<'a>> {
        self.conn.prepare(&format!(
            "INSERT INTO data ({}) VALUES ({});",
            self.columns.join(", "),
            iter::repeat("?").take(self.columns.len()).collect::<Vec<&str>>().join(", "),
        )).map_err(|e| e.into())
    }

    pub fn count(&self, condition: &str) -> SimpleResult<u32> {
        self.conn.query_row(
            &format!("SELECT COUNT(*) FROM data WHERE {};", condition),
            NO_PARAMS,
            |row| row.get(0),
        ).map_err(|e| e.into())
    }

    pub fn begin(&self) -> SimpleResult<()> {
        self.conn.execute("BEGIN;", NO_PARAMS).map(|_| ()).map_err(|e| e.into())
    }

    pub fn end(&self) -> SimpleResult<()> {
        self.conn.execute("END;", NO_PARAMS).map(|_| ()).map_err(|e| e.into())
    }
}
