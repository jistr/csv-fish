#!/bin/bash

set -euxo pipefail

DIR=$(cd $(dirname "${BASH_SOURCE[0]}") &> /dev/null; pwd)
ROOT_DIR=${ROOT_DIR:-$DIR/..}
TEST_DATA_DIR=${TEST_DATA_DIR:-$ROOT_DIR/example}
TEST_CACHE_DIR=${TEST_CACHE_DIR:-$DIR/cache}
FISH_BIN=${FISH_BIN:-$ROOT_DIR/target/debug/csv-fish}

rm -r "$TEST_CACHE_DIR" || true
mkdir "$TEST_CACHE_DIR"

function test_output() {
    "$FISH_BIN" --data "$TEST_DATA_DIR/data.csv" \
                --groups "$TEST_DATA_DIR/groups.csv" \
                --output "$TEST_CACHE_DIR/output.csv"

    diff "$TEST_DATA_DIR/expected-output.csv" "$TEST_CACHE_DIR/output.csv"
}

test_output
