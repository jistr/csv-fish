.PHONY: build clean test test-unit test-func
.DEFAULT_GOAL := build
SHELL := /bin/bash
ROOT_DIR := $(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))

# BUILD

BUILD_FLAGS ?= --release

build:
	cargo build $(BUILD_FLAGS)

clean:
	cargo clean


# TEST

test: test-unit test-func
	echo "ALL TESTS OK"

test-unit:
	cargo test && \
	echo "UNIT TESTS OK"

test-func:
	cargo build && \
	bash ./test-func/run.sh && \
	echo "FUNC TESTS OK"
